const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.students = [
    {
        name: "Gigel",
        surname: "Popel",
        age: 23
    },
    {
        name: "Gigescu",
        surname: "Ionel",
        age: 25
    }
];

app.get('/students', (req, res) => {
    res.status(200).json(app.locals.products);
});

app.post('/students', (req, res, next) => {
    const student = req.body;
    if(Object.keys(student).length === 0) {
        res.status(500).send({
            message: "Body is missing"
        })
    }
    if(student.name && student.surname && student.age) {
        if(student.age < 0) {
            res.status(500).send({
                message: 'Age should be a positive number'
            })
        } else if(app.locals.students.find(stud => stud.name === student.name)) {
            res.status(500).send({
                message: 'Student already exists'
            })
        } else {
             app.locals.students.push(student);
            res.status(201).send({
                message: 'Created'
            })
        }
    } else {
        res.status(500).send({
            message: 'Invalid body format'
        })
    }
    res.status(400).json({message: 'Bad request'});
})

module.exports = app;