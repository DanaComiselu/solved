const express = require("express")
const app = express()

//IMPORTANT: daca da eroare se poate ca JSON-ul sa contina erori. (lipsesc virgulele dintre atribute etc)
//iti importa node.js jsonul
const data = require('./account.json');
//pui asta ca sa mearga index.html
app.use(express.static('public'))

//creezi un nou endpoint in care pur si simplu trimiti datele 
app.get('/data',(req,res)=>{
    res.status(200).send(data);
})

//si dupa in browser daca dau IP:PORT/data imi da jsonul


//dupa aia te duci in index.html
app.listen(8080);


module.exports = app;